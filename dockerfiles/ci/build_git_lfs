#!/usr/bin/env sh

set -eo pipefail

# This script builds git-lfs from source and places the binary in
# /usr/local/bin. This is necessary to resolve a number of CVES vulnerabilities
# reported against this image. Note that this script clobbers the version of git-lfs
# installed via install-deps.
#
# We can probably remove this on the next release of git-lfs.
# See https://gitlab.com/gitlab-org/gitlab-runner/-/issues/31065

cd /tmp

downloadURL="https://github.com/git-lfs/git-lfs/archive/refs/tags/v${GIT_LFS_VERSION}.tar.gz"
wget "${downloadURL}" -O "git-lfs-${GIT_LFS_VERSION}.tar.gz"

tar -xzf "git-lfs-${GIT_LFS_VERSION}.tar.gz"
make -C "git-lfs-${GIT_LFS_VERSION}"
cp "git-lfs-${GIT_LFS_VERSION}/bin/git-lfs" /usr/bin/
rm -rf /tmp/*
git-lfs version
